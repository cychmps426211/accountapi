var mysql = require('mysql');
var conf = require('../conf');
 
var connection = mysql.createConnection(conf.db);
var sql = '';

module.exports = {
    item: function (req, callback) {
        sql = mysql.format('SELECT * FROM weichenbank WHERE trade_account = ?', [req.params.bankname]);
        return connection.query(sql, callback);
    },
	total: function (req, callback) {
        sql = mysql.format('SELECT (SELECT SUM(trade_money) as totalin FROM `weichenbank` WHERE `trade_type` = "收入" AND trade_account = ?) - (SELECT SUM(trade_money) as totalout FROM `weichenbank` WHERE `trade_type` = "支出" AND trade_account = ?) as totalMoney', [req.params.bankname, req.params.bankname]);
        return connection.query(sql, callback);
    }
};