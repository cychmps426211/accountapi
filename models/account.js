var mysql = require('mysql');
var conf = require('../conf');
 
var connection = mysql.createConnection(conf.db);
var sql = '';

module.exports = {
    item: function (req, callback) {
        sql = mysql.format('SELECT * FROM accountlist WHERE accname = ?', [req.params.id]);
        return connection.query(sql, callback);
    }
};