var express = require('express');
var banks = require('../models/bankQuery');
 
var router = express.Router();

// 獲取如 /bank/永豐 請求
router.route('/:bankname')
    // 取得指定的一筆資源
    .get(function (req, res) {
        banks.item(req, function (err, results, fields) {
            if (err) {
                res.sendStatus(500);
                return console.error(err);
            }
 
            if (!results.length) {
                res.sendStatus(404);
                return;
            }
 
            res.json(results);
        });
    });
	
// 獲取如 /bank/total/永豐 請求
router.route('/total/:bankname')
    // 取得指定的一筆資源
    .get(function (req, res) {
        banks.total(req, function (err, results, fields) {
            if (err) {
                res.sendStatus(500);
                return console.error(err);
            }
 
            if (!results.length) {
                res.sendStatus(404);
                return;
            }
 
            res.json(results);
        });
    });
    
module.exports = router;