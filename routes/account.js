var express = require('express');
var accounts = require('../models/account');
 
var router = express.Router();

// 獲取如 /accounts/1 請求
router.route('/:id')
    // 取得指定的一筆資源
    .get(function (req, res) {
        accounts.item(req, function (err, results, fields) {
            if (err) {
                res.sendStatus(500);
                return console.error(err);
            }
 
            if (!results.length) {
                res.sendStatus(404);
                return;
            }
 
            res.json(results);
        });
    });
    
module.exports = router;