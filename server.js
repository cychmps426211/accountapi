var bodyparser = require('body-parser');    // 解析 HTTP 請求主體的中介軟體

var conf = require('./conf');
var functions = require('./functions');
var accounts = require('./routes/account');
var banks = require('./routes/bankQuery');

const cors = require('cors');

const app = require('./app');

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

app.use(cors());
 
app.use(functions.passwdCrypto);
app.use('/account', accounts);

app.use('/banks', banks);

const port = process.env.PORT || 3333;

const server = app.listen(port, function () {
    console.log('Express started on port ' + port);
});
